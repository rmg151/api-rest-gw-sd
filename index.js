
'use strict' 

const port = process.env.PORT || 3100;
const URL_WS = "https://localhost:?/api";
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

//incorporar librerias:
const https = require('https');
const logger = require('morgan');
const express = require('express');
const fetch = require('node-fetch');
const fs = require('fs');
//const { Certificate } = require('crypto');


const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}; 


//declaramos nuestra aplicacion de tipo express:
const app = express(); 

//declaramos middleware:
app.use(logger('dev'));
app.use(express.urlencoded({extended: false})); //para leer objetos de formularios y poder entender el body de las diferentes r4equest
app.use(express.json()); //para reconocer objetos json

//Middleware de Autorizacion tipo beare:
function auth (req,res,next){
    if(!req.headers.authorization){
        res.status(403).json( {
            result: 'KO',
            mensaje: "No se ha enviado el token tipo Bearer en la cabecera Authorization."

        });
        return next(new Error("Falta token de autorizacion"));
    }

    const miToken = req.headers.authorization.split(" ")[1];
    if(miToken=="MITOKEN123456789"){
        req.params.token=miToken;
        return next();
    }

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio"
    });
    return next(new Error ("Acceso no autorizado"));
}


//Routes y Controllers:

/**
//Controlador que obtiene todas las colecciones de coches que hay
app.get('/api/', (request, response, next) => {
    console.log('GET /api');
    console.log(request.params); //estan todos los parametros que se pasan
    console.log(request.collection); //devuelve la coleccion a la que apunte

    //cuando alguien llame a /api, me imprimirá todas las colecciones que hay:
    db.getCollectionNames((err, colecciones) => {
        if(err) return next(err); //si hay error llamo a la siguiente funcion propagando el error
        console.log(colecciones);
        response.json({result: 'OK', colecciones: colecciones}); //sino devuelvo las colecciones
    });

});
*/


//obtener todos los objetos de una tabla
app.get('/api/:colecciones', (request, response, next) => {

    const queColeccion = request.params.colecciones; //Recoger las colecciones
    const queURL = `${URL_WS}/${queColeccion}`;

    //lamada de cliente a mi servicio:
    fetch( queURL )
        .then( response => response.json() )
        .then (json => {
            //mi logica de negocio:
            response.json( {
                result: 'OK',
                coleccion: queColeccion,
                elementos: json.elementos 
            });
    })
});



//Obtener un producto en concreto por el id:
app.get('/api/:colecciones/:id', (request, response, next) => {
    
    const queId = request.params.id;
    const queColeccion = request.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}/${queId}`;
    
    fetch( queURL )
    .then( response => response.json() )
    .then (json => {
        //mi logica de negocio:
        response.json( {
            result: 'OK',
            coleccion: queColeccion,
            elemento: json.elemento
        });
    })
    
 //   request.collection.findOne({_id: id(request.params.id)}, (err, coleccion) => {
 //   if (err) return next(err);
 //   response.json(coleccion);
 //  });
});


/** 
//crear un elemento:
app.post('/api/:colecciones', auth, (request, response, next) => {
    const nuevoElemento = request.body; 
    const queColeccion = request.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}`;
    const queToken = request.headers.authorization;

    fetch( queURL, {
                        method: 'POST',
                        body: JSON.stringify(nuevoElemento),
                        headers: { 'Content-Type': 'application/json',
                                    'Authorization': `Bearer MITOKEN123456789`
                                 }
                    })
    .then( response => response.json() )
    .then (json => {
        //mi logica de negocio:
        response.json( {
            result: 'OK',
            coleccion: queColeccion,
            nuevoElemento: json.elemento
        });
    })
   
});
*/


/** 

app.put('/api/:colecciones/:id', (request, response, next) => {
    const elementoId = request.params.id;
    const queColeccion = request.params.colecciones;
    const elementoNuevo = request.body;
    request.collection.update(
        {_id: id(elementoId)}, //convierte cadena de texto a identificador
        {$set: elementoNuevo}, //añadir el elemento nuevo
        {safe: true, multi: false}, (err, elementoModif) => {
            if (err) return next(err);
            console.log(elementoModif);
            response.json(elementoModif);
        });
}); 


app.delete('/api/:colecciones/:id', auth, (request, response, next) => {
    let elementoId = request.params.id;
    
    request.collection.remove({_id: id(elementoId)}, (err, resultado) => {
    if (err) return next(err);
    response.json(resultado);
    });
});

*/


https.createServer( opciones, app ).listen(port, () => {
    console.log(`API RESTFul CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

/**


app.listen(port, () => {
    console.log(`API RESTFul CRUD ejecutandose en http://localhost:${port}/api/{colecciones}/{id}`);
});

*/



